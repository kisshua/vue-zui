define("components/swiper/slide",function(require){

	var defaultTpl='<div class="vue-swiper-slide">{{{itemTpl}}}</div';

	var Slide={

		props:{
			initance:Number,
			activedIndex:Number,
			itemTpl:String
		},
		data:function(){
			return{
				initance:0,
				activedIndex:0,
				itemTpl:null
			}
		}

	};


	return Slide;
});